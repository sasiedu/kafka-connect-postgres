#!/bin/bash

# ============================================================
# Set version tag number
# ============================================================
TAG=$(git rev-parse --short HEAD)

# ============================================================
# Build image
# ============================================================
echo docker build -f Dockerfile --tag foundery.azurecr.io/foundery/kafka/manager:${TAG} .
docker build -f Dockerfile --tag foundery.azurecr.io/foundery/kafka/manager:${TAG} .

# ============================================================
# Add tags
# Note: $BUILD_NUMBER is a TeamCity environment variable
# ============================================================
#docker tag nexus.foundery.club/foundery/kafka/manager:${TAG} nexus.foundery.club/foundery/kafka/manager:${PRE}$(if [[ ${BUILD_NUMBER} ]]; then echo ${BUILD_NUMBER}; else echo "local"; fi)
#docker tag nexus.foundery.club/foundery/kafka/manager:${TAG} nexus.foundery.club/foundery/kafka/manager:${PRE}latest
#
#docker tag nexus.foundery.club/foundery/kafka/manager:${TAG} foundery.azurecr.io/foundery/kafka/manager:${TAG}
#docker tag nexus.foundery.club/foundery/kafka/manager:${TAG} foundery.azurecr.io/foundery/kafka/manager:${PRE}$(if [[ ${BUILD_NUMBER} ]]; then echo ${BUILD_NUMBER}; else echo "local"; fi)
docker tag foundery.azurecr.io/foundery/kafka/manager:${TAG} foundery.azurecr.io/foundery/kafka/manager:latest