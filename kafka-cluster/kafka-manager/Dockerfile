FROM openjdk:8u151-jdk-alpine3.7

# Default to UTF-8 file.encoding
ENV LANG C.UTF-8

RUN apk update
RUN apk upgrade
RUN apk add bash

# add a simple script that can auto-detect the appropriate JAVA_HOME value
# based on whether the JDK or only the JRE is installed
RUN { \
		echo '#!/bin/sh'; \
		echo 'set -e'; \
		echo; \
		echo 'dirname "$(dirname "$(readlink -f "$(which javac || which java)")")"'; \
	} > /usr/local/bin/docker-java-home \
	&& chmod +x /usr/local/bin/docker-java-home

ENV JAVA_HOME /usr/lib/jvm/java-1.8-openjdk
ENV JAVA_VERSION 8u151
ENV JAVA_ALPINE_VERSION 8.151.12-r0
ENV PATH $PATH:/usr/lib/jvm/java-1.8-openjdk/jre/bin:/usr/lib/jvm/java-1.8-openjdk/bin

ENV ZK_HOSTS=localhost:2181
ENV KM_VERSION=1.3.3.16
ENV KM_REVISION=6cf43e383377a6b37df4faa04d9aff515a265b30
ENV KM_CONFIGFILE="conf/application.conf"

ADD start-kafka-manager.sh /kafka-manager-${KM_VERSION}/start-kafka-manager.sh

RUN apk add --no-cache git wget unzip which curl
RUN mkdir -pv /tmp && \
	cd /tmp && \
	git clone https://github.com/yahoo/kafka-manager

RUN cd /tmp/kafka-manager && \
	echo 'scalacOptions ++= Seq("-Xmax-classfile-name", "200")' >> build.sbt && \
	./sbt clean dist && \
	unzip -d / ./target/universal/kafka-manager-${KM_VERSION}.zip && \
	rm -fr /tmp/* /root/.sbt /root/.ivy2 && \
	printf '#!/bin/sh\nexec ./bin/kafka-manager -Dconfig.file=${KM_CONFIGFILE} "${KM_ARGS}" "${@}"\n' > /kafka-manager-${KM_VERSION}/km.sh && \
	chmod +x /kafka-manager-${KM_VERSION}/start-kafka-manager.sh && \
	rm -fr /kafka-manager-${KM_VERSION}/share \
	apk del git

WORKDIR /kafka-manager-${KM_VERSION}

EXPOSE 9000
ENTRYPOINT ["./start-kafka-manager.sh"]
