#/bin/sh

echo "[Creating namespace]"
kubectl apply -f ../messaging-namespace.yaml

echo "[Applying messaging secret]"
kubectl apply -f ../messaging-private-registry-secret.yaml --namespace="messaging"

echo "[Creating kafka-manager service]"
kubectl apply -f manager-svc.yaml
echo "[Creating kafka-manager service completed]"

echo "[Creating kafka-manager deployment]"
kubectl apply -f manager-deployment.yaml
echo "[Creating kafka-manager deployment completed]"

